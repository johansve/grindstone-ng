#-------------------------------------------------
#
# Project created by QtCreator 2024-03-07T10:24:55
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GrindStone
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    report.cpp \
    database.cpp

HEADERS  += mainwindow.h \
    report.h \
    database.h

FORMS    += mainwindow.ui \
    report.ui
