#ifndef REPORT_H
#define REPORT_H

#include <QDialog>

#include <QTableView>
#include <QItemDelegate>
#include <QStandardItemModel>
#include <QtSql/QSqlDatabase>
#include <QList>
#include <QPushButton>
#include <QTimer>
#include <QTableWidgetItem>

namespace Ui {
class Dialog;
}

class Report : public QDialog
{
    Q_OBJECT

public:
    explicit Report(QWidget *parent = 0);
    ~Report();
    void test();
private:
    Ui::Dialog *ui;
    QStandardItemModel *model;

};

#endif // Report_H
