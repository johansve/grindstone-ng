#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QTableView>
#include <QItemDelegate>
#include <QStandardItemModel>
#include <QList>
#include <QPushButton>
#include <QTimer>
#include <QTableWidgetItem>

namespace Ui {
class MainWindow;
}
class Sql_data;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


protected:
    bool eventFilter(QObject* obj, QEvent* event);
private:
    Ui::MainWindow *ui;
    QStandardItemModel *model;
    QList<Sql_data> items;
    QList<QPushButton*> pushButtons;
    QList<QPushButton*> deleteButtons;
    QPushButton* active;
    int activeNr;
    QTimer* timer;

    void showReport();
    void exitApplication();
    void addEntry();
    void create2();
    void fetchData();
    void addEntries(Sql_data& d);
    void toggle(QPushButton* button);
    void stopAll();
    void MyTimerSlot();
    int findNr(QPushButton *button);

    public slots:
    void onDataChanged(const QModelIndex& oldItem, const QModelIndex& newItem);
};

#endif // MAINWINDOW_H
