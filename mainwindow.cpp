#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QKeyEvent>
#include <QDebug>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QTime>
#include <QDebug>
#include <QTableWidget>
#include "report.h"
#include "database.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    timer(nullptr)
{
    ui->setupUi(this);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::exitApplication);
    connect(ui->pushButtonAdd, &QPushButton::pressed, this, &MainWindow::addEntry);
    connect(ui->actionReport, &QAction::triggered, this, &MainWindow::showReport);

    //ui->centralWidget->installEventFilter(this);

    model = new QStandardItemModel(0,10,this);
    model->setHeaderData(0, Qt::Horizontal, tr("Tidkod"));
    model->setHeaderData(1, Qt::Horizontal, tr("Måndag"));
    model->setHeaderData(2, Qt::Horizontal, tr("Tisdag"));
    model->setHeaderData(3, Qt::Horizontal, tr("Onsdag"));
    model->setHeaderData(4, Qt::Horizontal, tr("Torsdag"));
    model->setHeaderData(5, Qt::Horizontal, tr("Fredag"));
    model->setHeaderData(6, Qt::Horizontal, tr("Lördag"));
    model->setHeaderData(7, Qt::Horizontal, tr("Söndag"));
    model->setHeaderData(8, Qt::Horizontal, tr("Stop"));
    model->setHeaderData(9, Qt::Horizontal, tr("Remove"));

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->setModel(model);

    fetchData();

    //connect(ui->tableView,  &QTableWidget::itemChanged, this, &MainWindow::test);
    //connect(ui->tableView, &QTableWidget::update, this, &MainWindow::update);
    //connect(ui->tableView, &QTableWidget::itemChanged, this, &MainWindow::on_table_itemChanged);
    connect(ui->tableView->model(), SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));
}

void MainWindow::onDataChanged(const QModelIndex& oldItem, const QModelIndex& newItem) {

    qDebug() << "!HA";
    int row = newItem.row();
    int col = newItem.column();
    int newVal = newItem.data().toInt();

    switch(col) {
    case 0: //timecode
        items[row].week.timecode = newItem.data().toString();
        break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
        items[row].worked[col - 1].worked_minutes = newVal;
        break;
    default:
        return;
    }
    Database::getInstance()->update(items[row]);
}


void MainWindow::fetchData() {
    Sql_data data;
    data.week.week = QDate::currentDate().weekNumber();
    QList<Sql_data> items = Database::getInstance()->getWeek(data);

    for(Sql_data &item : items)
        addEntries(item);
}

int MainWindow::findNr(QPushButton *button) {
    for(int i = 0; i < pushButtons.size(); ++i)
        if(button == pushButtons[i]) return i;

    return -1;
}

void MainWindow::addEntries(Sql_data& data) {

    QList<QStandardItem*> newRow;
    // These might need to be freed if the QT framework doesn't do it.
    QStandardItem *item1 = new QStandardItem(data.week.timecode);
    for(int i = 0 ; i < WEEK_DAYS; ++i) {
        QStandardItem *item = new QStandardItem(QString::number(data.worked[i].worked_minutes));
        newRow.append(item);
    }

    model->appendRow(newRow);
    ui->tableView->setModel(model);

    QPushButton* startButton = new QPushButton();
    startButton->setText("Stopped");
    ui->tableView->setIndexWidget(model->index(items.size() , 8), startButton);
    pushButtons.append(startButton);

    connect(startButton, &QPushButton::clicked, this, [=]() {
        toggle(startButton);
    });

    QPushButton* deleteButton = new QPushButton();
    deleteButton->setText("Delete");
    ui->tableView->setIndexWidget(model->index(items.size() , 9), deleteButton);

    connect(deleteButton, &QPushButton::clicked, this, [=]() {
        for(int i = 0; i < deleteButtons.size(); ++i) {
            if(deleteButtons[i] != deleteButton)
                continue;

            ui->tableView->model()->removeRow(i);

            QPushButton *d = deleteButtons.at(i);
            QPushButton *p = pushButtons.at(i);
            deleteButtons.removeAt(i);
            pushButtons.removeAt(i);
            delete d;
            delete p;

            // Remove item from database
            Database::getInstance()->remove(items.at(i));
            items.removeAt(i);

        }
    });

    deleteButtons.append(deleteButton);
    items.append(data);
}

void MainWindow::stopAll() {
    QFont f;
    f.setBold(false);
    f.setPixelSize(12);
    for(auto& button : pushButtons) {
        button->setText("Stopped");
        button->setFont(f);
    }
}

void MainWindow::toggle(QPushButton* button) {
    stopAll();
    if(timer != nullptr) {
        timer->stop();
        delete timer;
        timer = nullptr;
    }

    if(active == button) {
        active = nullptr;
        return;
    }

    button->setText("Started");
    active = button;
    activeNr = findNr(button);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::MyTimerSlot);
    timer->start(1000*60); // 60 sec

    QFont f;
    f.setBold(true);
    f.setPixelSize(20);
    button->setFont(f);
}

void MainWindow::MyTimerSlot()
{
    int offset = 1;
    int weekday = QDate::currentDate().dayOfWeek() - 1;
    int column = offset + weekday;
    int val = ui->tableView->model()->data(ui->tableView->model()->index(activeNr, column)).toInt() + 1;
    QString s = QString::number(val);
    ui->tableView->model()->setData(model->index(activeNr , column) ,  s);
    items[activeNr].worked[weekday].worked_minutes = val;
    Database::getInstance()->update(items[activeNr]);
}


void MainWindow::exitApplication() {
    qApp->exit();
}

void MainWindow::showReport() {
    Report r;
    r.show();
    r.exec();
}

void MainWindow::addEntry() {
    Sql_data data = {};

    data.week.timecode = "new timecode";
    data.week.week     = QDate::currentDate().weekNumber();
    data.week.month    = QDate::currentDate().month();
    data.week.year     = QDate::currentDate().year();

    Sql_data ret = Database::getInstance()->insert(data);
    addEntries(ret);
}

bool MainWindow::eventFilter(QObject* obj, QEvent* event)
{
    qDebug() << event->type();
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent* keyEvent = static_cast<QKeyEvent *>(event);
        qDebug() << "Ate key press " << keyEvent->text();
        return true;
    }
    else if(event->type() == QEvent::MouseButtonPress)
    {
        qDebug() << "Mouse press detected";
        return true;
    }
    // standard event processing
    return QObject::eventFilter(obj, event);
}

MainWindow::~MainWindow()
{
    delete ui;
}



