#include "report.h"
#include "ui_report.h"
#include <QDate>
#include <QDebug>
#include "database.h"
Report::Report(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)

{
    ui->setupUi(this);

    connect(ui->pushButtonSubtractWeek, &QPushButton::clicked, this, [=]() {
        int val = ui->labelWeek->text().toInt();
        val--;
        ui->labelWeek->setText(QString::number(val));
        test();
    });

    connect(ui->pushButtonAddWeek, &QPushButton::clicked, this, [=]() {
        int val = ui->labelWeek->text().toInt();
        val++;
        ui->labelWeek->setText(QString::number(val));
        test();
    });

    connect(ui->pushButtonClose, &QPushButton::clicked, this, [=]() {
        close();
    });


    int week = QDate::currentDate().weekNumber();
    ui->labelWeek->setText(QString::number(week));

    Sql_data data;
    data.week.week = QDate::currentDate().weekNumber();
    QList<Sql_data> items = Database::getInstance()->getReportWeek(data);
    for(Sql_data& item : items) {
        ui->plainTextEdit->appendPlainText(item.toString());
    }
}

void Report::test() {

    ui->plainTextEdit->setPlainText("");

    QString weekNo = ui->labelWeek->text();
    Sql_data data;
    data.week.week = weekNo.toInt();

    QList<Sql_data> items = Database::getInstance()->getReportWeek(data);

    for(Sql_data& item : items) {
        ui->plainTextEdit->appendPlainText(item.toString());
    }
}


Report::~Report()
{
    delete ui;
}

