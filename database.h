#ifndef DATABASE_H
#define DATABASE_H

#include <QString>
#include <QList>
#include <QSqlDatabase>

#define WEEK_DAYS 7

struct data_week {
    int id;
    QString timecode;
    int week;
    int month;
    int year;
};

struct data_worked {
    int id;
    int week_id;
    int week_day;
    int worked_minutes;
};


struct Sql_data {
    struct data_week week;
    struct data_worked worked[WEEK_DAYS];
    QString toString();
};

class Database
{
private:
    static Database* instancePtr;

    Database();
    ~Database();
    void create();

    QSqlDatabase db;

    Sql_data getItem(Sql_data data);

public:
    static Database* getInstance();
    Database(const Database& obj) = delete;

    QList<Sql_data> getWeek(Sql_data data);
    void update(Sql_data data);
    void remove(Sql_data data);
    Sql_data insert(Sql_data data);
    QList<Sql_data> getReportWeek(Sql_data data);



};
#endif // DATABASE_H
