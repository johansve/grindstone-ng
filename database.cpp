#include "database.h"
#include <QSqlQuery>
#include <QVariant>
#include <QFile>
#include <QDebug>
#include <QSqlError>

Database* Database::instancePtr = nullptr;

Database::Database() {
    QString path = "./database.db";
    bool notFound = !QFile::exists(path);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
    db.open();

    if(notFound)
        create();

}

Database::~Database() {

}

Database* Database::getInstance() {
    if (instancePtr == nullptr)
        instancePtr = new Database();

    return instancePtr;
}

QList<Sql_data> Database::getWeek(Sql_data data) {
    QList<Sql_data> ret;

    QSqlQuery sqlQuery;

    sqlQuery.prepare("SELECT * from week where week = :week");
    sqlQuery.bindValue(":week", data.week.week);
    sqlQuery.exec();

    while (sqlQuery.next()) {
        Sql_data data;
        data.week.id = sqlQuery.value("id").toInt();
        data.week.timecode = sqlQuery.value("timecode").toString();
        data.week.week = sqlQuery.value("week").toInt();
        data.week.year = sqlQuery.value("year").toInt();

        QSqlQuery sqlQuery2;
        sqlQuery2.prepare("select * from worked where week_id = :week_id");
        sqlQuery2.bindValue(":week_id", data.week.id);
        sqlQuery2.exec();
        int i = 0;
        while (sqlQuery2.next()) {
            data.worked[i].id = sqlQuery2.value("id").toInt();
            data.worked[i].week_id = sqlQuery2.value("week_id").toInt();
            data.worked[i].week_day = sqlQuery2.value("week_day").toInt();
            data.worked[i].worked_minutes = sqlQuery2.value("worked_minutes").toInt();
            i++;
        }
        ret.append(data);
    }

    return ret;
}

void Database::create() {
    QSqlQuery query;
    query.exec("create table week ("
               "id             integer primary key, "
               "timecode       varchar(80), "
               "week           integer NOT NULL,"
               "month          integer NOT NULL,"
               "year           integer NOT NULL"
               ")");

    query.exec("create table worked ("
               "id            integer primary key, "
               "week_id        integer NOT NULL,"
               "week_day       integer NOT NULL,"
               "worked_minutes integer NOT NULL"
               ")");
}

void Database::update(Sql_data data) {
    QSqlQuery query;
    query.prepare("UPDATE week "
                  "set timecode = :timecode,"
                  "week         = :week,"
                  "month        = :month, "
                  "year         = :year "
                  "where id     = :id");

    query.bindValue(":id", data.week.id);
    query.bindValue(":timecode", data.week.timecode);
    query.bindValue(":week", data.week.week);
    query.bindValue(":month", data.week.month);
    query.bindValue(":year", data.week.year);
    query.exec();

    for(int i = 0; i < WEEK_DAYS; ++i) {
        QSqlQuery query2;
        query2.prepare("UPDATE worked "
                      "set week_id    = :week_id,"
                      "week_day       = :week_day,"
                      "worked_minutes = :worked_minutes "
                      "where id       = :id");

        query2.bindValue(":id", data.worked[i].id);
        query2.bindValue(":week_id", data.worked[i].week_id);
        query2.bindValue(":week_day", data.worked[i].week_day);
        query2.bindValue(":worked_minutes", data.worked[i].worked_minutes);
        query2.exec();
    }
}

void Database::remove(Sql_data data) {

    QSqlQuery query;
    query.prepare("DELETE FROM week where id = :id");
    query.bindValue(":id", data.week.id);
    query.exec();

    QSqlQuery query2;
    query2.prepare("DELETE FROM worked where week_id = :id");
    query2.bindValue(":id", data.week.id);
    query2.exec();

}

Sql_data Database::insert(Sql_data data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO week("
                  "timecode,"
                  "week,"
                  "month,"
                  "year"
                  ") values("
                  ":timecode, "
                  ":week,"
                  ":month,"
                  ":year)");
    query.bindValue(":timecode", data.week.timecode);
    query.bindValue(":week", data.week.week);
    query.bindValue(":month", data.week.month);
    query.bindValue(":year", data.week.year);
    query.exec();

    int id = query.lastInsertId().toInt();
    data.week.id = id;


    for(int i = 0; i < WEEK_DAYS; ++i) {
        QSqlQuery query2;
        query2.prepare("INSERT INTO worked ("
                       "week_id,"
                       "week_day,"
                       "worked_minutes"
                       ") values("
                       ":week_id,"
                       ":week_day,"
                       ":worked_minutes"
                       ")");

        query2.bindValue(":week_id", id);
        query2.bindValue(":week_day", i);
        query2.bindValue(":worked_minutes", 0);
        query2.exec();

        data.worked[i].id = query2.lastInsertId().toInt();
        data.worked[i].week_id = id;
        data.worked[i].week_day = i;
        data.worked[i].worked_minutes = 0;

    }
    return data;
}

QList<Sql_data> Database::getReportWeek(Sql_data data) {
    QList<Sql_data> ret = getWeek(data);

    for(int i = ret.size()-1; i >= 0 ; --i) {
        bool keep = false;
        for(int j = 0; j < WEEK_DAYS; ++j) {
            if(ret[i].worked[j].worked_minutes > 0) {
                keep = true;
                break;
            }
        }
        if(!keep)
            ret.removeAt(i);
    }
    return ret;
}

QString Sql_data::toString()
{
    QString worked_time;
    for(int i = 0; i < WEEK_DAYS; ++i) {
        int worked_minutes = worked[i].worked_minutes;
        int hours = worked_minutes/60;
        int minutes = worked_minutes - 60*hours;
        minutes = (minutes/60.0)*100;
        worked_time += "[" + QString::number(hours) + "," + QString::number(minutes) + "] ";
    }
    return week.timecode + ": " + worked_time;
}

